# Escribe un programa donde indique si es o no un numero primo ”.

#__Autor:_ Angel Bolivar Contento Guamán
#__Email__ angel.b.contento@unl.edu.ec


def num_esprimo (N):
    if N<2:
        return False
    for i in range(2, N):
        if N % i ==0:
            return False
        else:
            return True

def categorizacion ():

    N= int(input("Ingresa el número:\n"))
    resultado= num_esprimo(N)

    if resultado is True:
        print ("El número es primo porque es divisible para si mismo y para 1")

    else:
        print ("El número no es primo porque es divisibe para otros numeros")

if __name__ =='__main__':
    categorizacion()
